module Main exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Player

type Msg = 
    NoOp |
    Player Player.Msg

type alias Model = {
    player : Player.Model,
    count: Int
}

view: Model -> Html Msg
view model = 
    div [class "container"] [
        div [class "dv"] [text("hello world!!")],
        Html.map Player (Player.view model.player)
    ]

update: Msg -> Model -> Model
update msg model = 
    model

model: Model
model = {
        player = Player.model,
        count = 1
    }    

main : Program Never Model Msg
main =
    Html.beginnerProgram { model = model, view = view, update = update }
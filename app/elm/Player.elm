module Player exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

type alias Model = {
    currentUrl: String
}

type Msg = NoOp

model: Model
model = {currentUrl = "/nomedia"}

update: Msg -> Model -> Model
update msg model = 
    model

view: Model -> Html Msg
view model = 
    div [] [text(model.currentUrl)]

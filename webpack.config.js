var path              = require('path');
var webpack           = require('webpack');
var merge             = require('webpack-merge');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var UglifyJSPlugin    = require('uglifyjs-webpack-plugin');
var entryPath         = path.join(__dirname, 'app/main.js');
var outputPath        = path.join(__dirname, 'dist');
var TARGET_ENV 		  = process.env.npm_lifecycle_event === 'build' ? 'production' : 'development';

// common webpack config
var commonConfig = {
	module: {
		noParse: /\.elm$/,
		loaders: [{
				test: /\.(eot|ttf|woff|woff2|svg)$/,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			test: /\.html$/,
			options: {
				template: 'app/index.html',
				filename: 'index.html'
			}
		})
	]
}

// npm start
if (TARGET_ENV === 'development') {
	console.log( 'Serving locally...');

	module.exports = merge(commonConfig, {
		entry: [
			'webpack-dev-server/client?http://localhost:8080',
			entryPath
		],
		devServer: {
			contentBase: './src',
		},
		module: {
			loaders: [
				{
					test:    /\.elm$/,
					exclude: [/elm-stuff/, /node_modules/],
					loader:  'elm-hot-loader!elm-webpack-loader?verbose=true&warn=true&debug=true'
				},
				{
					test: /\.(css|scss)$/,
					loaders: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				}
			]
		}
	});
}

// npm run build
if (TARGET_ENV === 'production') {
	console.log('Building prod...');

	module.exports = merge(commonConfig, {
		entry: entryPath,
		output: {
			path: outputPath,
			filename: 'js/[name].bundle.js',
		},
		module: {
			loaders: [{
					test: /\.elm$/,
					exclude: [/elm-stuff/, /node_modules/],
					loader: 'elm-webpack-loader'
				},{
					test: /\.(css|scss)$/,
					loaders: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				}
			]
		},
		plugins: [
			new UglifyJSPlugin()
		]
	});
}